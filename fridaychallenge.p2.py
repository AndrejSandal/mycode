import requests

# the URL you wish to post to
url = 'http://0.0.0.0:2224/login'

# the data you wish to post
ye_olde_dict = {'nm': '42'}

# post the data to the URL, capture the response as "x"
x = requests.post(url, json = ye_olde_dict)

# print the text response you get back to confirm all is well
print(x.text)
